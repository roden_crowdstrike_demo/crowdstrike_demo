#!/usr/bin/env python3
from fetch import Fetch
import config_container
import sys

url = sys.argv[1] if len(sys.argv) == 2 else 'http://www.reddit.com'

fetch = config_container.get_container().resolve_by_type(Fetch)
result = fetch.get(url)

print("Url {result.url}".format(result=result))
print("Title {result.title}".format(result=result))
for h1 in result.h1s:
    print("H1 {h1}".format(h1=h1))
for link in result.links:
    print("Link {link.text} ({link.title}) {link.href}".format(link=link))
