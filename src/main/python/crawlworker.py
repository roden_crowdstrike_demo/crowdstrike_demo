"""
    Given root URL follows all links. Duplicate paths are not followed.

    input
        URL(s)
    work
        fetch
        parse
        store  key=(domain,path)
    decision
        set interest level
        refresh url?
        queue

    Derived requirements
        refresh requires storage
"""
from threading import Thread


class CrawlWorker(Thread):
    """
        Works until queue signals quit
    """
    def __init__(self, fetch, queue, store):
        self._queue = queue
        self._fetch = fetch
        self._store = store

    def run(self):
        while True:
            next_item = self._queue.get()
            if next_item == 'die':
                self._queue.put('die')
                return
            result = self._fetch.get(next_item.data)
            self._store.upsert(result)
