from fetch import FetchResult
import sqlite3

# lacks primary keys indexes etc...
create_schema = """
create table page_link (contained_page_id text, link_page_id text);

create table page (
  page_id text,
  url text,
  domain text
);

create table page_scan(
  page_id text,
  scan_date INTEGER,
  title text,
  body text,
  h1s text,
  h2s text
);
"""


class CrawlStore:
    def __init__(self, db_url):
        self.conn = sqlite3.connect(db_url, uri=True)
        self.database_init()

    def upsert(self, fetch_result: FetchResult):
        self.conn.execute('')

    def database_init(self):
        self.conn.execute(create_schema)
        self.conn.commit()
