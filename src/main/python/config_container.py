import requests
import fetch
from rodenioc import Container


def get_container():
    container = Container()
    container.register(fetch.Fetch)
    container.register_instance(requests, 'requests')
    container.register_instance('data/crawl.db', 'db_file')
    return container
