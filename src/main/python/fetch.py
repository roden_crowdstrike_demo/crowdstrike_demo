from collections import namedtuple
from html.parser import HTMLParser
import time
from urllib import parse

FetchResult = namedtuple('FetchResult',
                         ['links', 'title', 'body', 'h1s', 'h2s', 'url',
                          'date'])


class Link:
    """
        DTO for links
    """

    def __init__(self, href, title, text=""):
        self.href = href
        self.title = title
        self.text = text


class Fetch:
    """
        Responsible for fetching a URL
    """

    def __init__(self, requests):
        self.__requests = requests

    def get(self, url) -> FetchResult:
        # TODO error handling
        parser = self.FetchParser(url)
        parser.feed(self.__requests.get(url).text)
        return parser.as_fetch_result(url)

    class FetchParser(HTMLParser):
        """
            Responsible parsing out meaningful html. Simple boolean state
            machine as built-in API encourages this style.
        """

        def __init__(self, base_url):
            super().__init__()
            self.base_url = base_url
            self.links = []
            self.title = ""
            self.body = []
            self.h1s = []
            self.h2s = []
            self.in_a = False
            self.in_h1 = False
            self.in_h2 = False
            self.in_body = False
            self.in_script = False
            self.in_title = False

        def as_fetch_result(self, url) -> FetchResult:
            # filter links without context and url
            # then covert to be absolute
            links = [Link(self.absolute_base(l.href), l.title, l.text)
                     for l in self.links if l.href
                     and (l.text or l.title)
                     and not l.href.startswith('#')
                     and not l.href.startswith('javascript')]
            return FetchResult(links, self.title, ''.join(self.body),
                               self.h1s, self.h2s, url, time.time())

        def handle_starttag(self, tag, attrs):
            if tag == "a":
                self.in_a = True
                href = next(iter(
                    [attrValue for (attrName, attrValue)
                     in attrs if attrName == 'href']),
                    None)
                title = next(iter(
                    [attrValue for (attrName, attrValue)
                     in attrs if attrName == 'title']),
                    None)
                self.links.append(Link(href, title))
            elif tag == "base":
                href = next(iter(
                    [attrValue for (attrName, attrValue)
                     in attrs if attrName == 'href']),
                    None)
                if href:
                    # parse base href for both // and relative assignment
                    self.base_url = self.absolute_base(href)
            elif tag == "h1":
                self.h1s.append("")
                self.in_h1 = True
            elif tag == "h2":
                self.h2s.append("")
                self.in_h2 = True
            elif tag == "body":
                self.in_body = True
            elif tag == "script":
                self.in_script = True
            elif tag == "title":
                self.in_title = True

        def handle_endtag(self, tag):
            if tag == "a":
                self.in_a = False
            if tag == "body":
                self.in_body = False
            if tag == "script":
                self.in_script = False
            elif tag == "h1":
                self.in_h1 = False
            elif tag == "h2":
                self.in_h2 = False
            elif tag == "title":
                self.in_title = False

        def handle_data(self, data):
            if self.in_script:
                return

            # bug? comments are captured when inline in sections

            if self.in_a:
                self.links[-1].text += data

            if self.in_h1:
                self.h1s[-1] += data

            if self.in_h2:
                self.h2s[-1] += data

            if self.in_title:
                self.title += data

            if self.in_body:
                self.body.append(data)

        def absolute_base(self, link):
            result = parse.urljoin(self.base_url, link)
            return result
