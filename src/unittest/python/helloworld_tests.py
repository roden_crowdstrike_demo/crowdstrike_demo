from unittest.mock import MagicMock
import unittest

from hello_world import hello_world


class HelloWorldTest(unittest.TestCase):
    def test_should_issue_hello_world_message(self):
        out = MagicMock()
        hello_world(out)
        out.write.assert_called_with("hello world!")
