from unittest.mock import MagicMock
import unittest
from fetch import Fetch


class ExampleData:
    def __init__(self, data):
        self.text = data


class FetchTest(unittest.TestCase):
    def test_get_should_call_url(self):
        requests = MagicMock()
        requests.get = MagicMock(return_value=ExampleData('<html> </html>'))
        fetch = Fetch(requests)
        result = fetch.get("http://example.com")
        requests.get.assert_called_with("http://example.com")
        self.assertEqual(result.url, "http://example.com")
        self.assertNotEquals(result.date, None)

    def test_get_should_find_title(self):
        requests = MagicMock()
        requests.get = MagicMock(return_value=ExampleData('<title>Foo</title>bar'))
        fetch = Fetch(requests)
        result = fetch.get("")
        self.assertEqual(result.title, "Foo")

    def test_get_should_have_body(self):
        requests = MagicMock()
        requests.get = MagicMock(return_value=ExampleData("""
          <title>Foo</title>
          bar
          <body>hello world</body>
          bar
        """))
        fetch = Fetch(requests)
        result = fetch.get("")
        self.assertEqual(result.body, "hello world")

    def test_get_should_ignore_script(self):
        requests = MagicMock()
        requests.get = MagicMock(return_value=ExampleData("""
          <title>Foo</title>
          bar
          <body>hello <script>foobar</script>world</body>
          bar
        """))
        fetch = Fetch(requests)
        result = fetch.get("")
        self.assertEqual(result.body, "hello world")

    def test_get_should_find_h1_in_body(self):
        requests = MagicMock()
        requests.get = MagicMock(return_value=ExampleData("""
          <title>Foo</title>
          bar
          <body><h1>hello <script>foobar</script>world</h1></body>
          <h1>bar</h1>
        """))
        fetch = Fetch(requests)
        result = fetch.get("")
        self.assertEqual(result.body, "hello world")
        self.assertEqual(result.h1s[0], "hello world")
        self.assertEqual(result.h1s[1], "bar")
        self.assertEqual(len(result.h1s), 2)

    def test_get_should_find_h2(self):
        requests = MagicMock()
        requests.get = MagicMock(return_value=ExampleData("""
          <title>Foo</title>
          bar
          <body><h2>hello <script>foobar</script>world</h2></body>
          <h1>bar</h1>
        """))
        fetch = Fetch(requests)
        result = fetch.get("")
        self.assertEqual(result.h2s[0], "hello world")

    def test_get_should_find_h2(self):
        requests = MagicMock()
        requests.get = MagicMock(return_value=ExampleData("""
          <title>Foo</title>
          bar
          <body><h2>hello <script>foobar</script>world</h2></body>
          <h1>bar</h1>
        """))
        fetch = Fetch(requests)
        result = fetch.get("")
        self.assertEqual(result.h2s[0], "hello world")

    def test_get_should_find_links(self):
        requests = MagicMock()
        requests.get = MagicMock(return_value=ExampleData("""
          <base href="plus/" />
          <body><h1>hello world</h1>
          <a href="http://foo.com" title="bar">foobar</a>
          <a href="foo2" title="bar2">foobar2</a>
          <a href="foo3">foo3</a>
          <a>foo<script>ignored</script>4</a>
          </body>
        """))
        fetch = Fetch(requests)
        result = fetch.get("http://example.com")
        self.assertEqual(result.links[0].href, "http://foo.com")
        self.assertEqual(result.links[0].title, "bar")
        self.assertEqual(result.links[0].text, "foobar")
        self.assertEqual(result.links[1].href, "http://example.com/plus/foo2")
        self.assertEqual(result.links[1].title, "bar2")
        self.assertEqual(result.links[1].text, "foobar2")
        self.assertEqual(result.links[2].href, "http://example.com/plus/foo3")
        self.assertEqual(result.links[2].title, None)
        self.assertEqual(result.links[2].text, "foo3")
        self.assertEqual(len(result.links), 3)

    def test_get_should_have_full_content_body(self):
        requests = MagicMock()
        requests.get = MagicMock(return_value=ExampleData("""
          <body><h1>hello world</h1>
          <a href="foo" title="bar">foobar</a>
          <a href="foo2" title="bar2">foobar2</a>
          <a href="foo3">foo3</a>
          <a>foo<script>ignored</script>4</a>
          </body>
        """))
        fetch = Fetch(requests)
        result = fetch.get("")
        self.assertEqual(result.body, """hello world
          foobar
          foobar2
          foo3
          foo4
          """)
