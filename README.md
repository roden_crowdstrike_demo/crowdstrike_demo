#Technical items of note

**General**

    * unit test coverage is very high

`rodenioc.py`
    
    * Implementation of sane IoC container in python. No results in 
      google/open-source capture coupling of Folwer's point. Most examples
      are just service locator patterns instead of loosely coupled IoC 
      containers that feel python (to me).
    * Usage is meant that at startup a configuration file creates and sets 
      a Container.
    * Then container.resolve(type) and off to the races...
    * Supported registrations types 
```
temporal = "New instance for every resolve",
singleton = "Only one instance ever, lazy constructed",
weak_reference = "Similar to singleton but object instance can be " \
                 "released if no usage is taking place before collection ",
thread_local = "Effectively singleton per thread. Useful for non-thread " \
               "safe code."
```
            
`fetch.py`

    * Light python3; `next(iter([attrValue for (attrName, attrValue) in attrs if attrName == 'href']), None)`
    * Usage of efficient string concat for body

```
project.build_depends_on('numpy')
project.build_depends_on('scipy')
project.build_depends_on('scikit-learn') 
```

#Install
Developed with python3.5 on mac/linux

##Requires valid fortran compiler
for simple mac install of gfortan

```
## ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew install gcc
```

##Virtualenv
Strongly recommend usage of virtualenv

```
pip3 install virtualenv
virtualenv --python=python3 . 
source bin/activate
```

##Build tool pybuilder
Uses open source pybuilder as build script, in-lue of different tools for 
dependency/packaging/testing/coverage etc.

```
pip install pybuilder
```

##First run
This will download the world and can take minutes to install dependencies.
Once done will lint, unit tests, and package.

```
pyb
```

##Scripts
Most functionality is exposed as scripts in `target/dist/**/scripts/*.py`


##Bin Packaging
Not tested...

```
target/dist/crawl_demo*/setup.py
```

##IDE support included
PyCharms or other IDE project files are generated....

```
pyb pycharm_generate
```

